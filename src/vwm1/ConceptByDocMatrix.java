package vwm1;

import java.util.Arrays;
import org.ejml.data.DenseMatrix64F;
import org.ejml.factory.DecompositionFactory;
import org.ejml.interfaces.decomposition.SingularValueDecomposition;
import org.ejml.simple.SimpleMatrix;

public class ConceptByDocMatrix {
    
    private DenseMatrix64F U;
    private DenseMatrix64F V;
    private DenseMatrix64F S;
    
    public ConceptByDocMatrix ( TermByDocMatrix termByDoc ) {
        SimpleMatrix A = termByDoc.createSimpleMatrix();
        SingularValueDecomposition<DenseMatrix64F> SVD = DecompositionFactory.svd(A.numRows(), A.numCols(), true, true, false);
        SVD.decompose(A.getMatrix());
        U = SVD.getU(null, true);
        V = SVD.getV(null, true);
        S = SVD.getW(null);
        S = sortMatrixS(S);
    }
    
    private DenseMatrix64F sortMatrixS(DenseMatrix64F S) {
        int dimension = S.getNumRows() < S.getNumCols() ? S.getNumRows() : S.getNumCols();
        double[] arrayToSort = new double[dimension];
        
        for ( int i = 0; i < dimension; i++ ) {
            arrayToSort[i] = S.get(i, i);
        }
        
        Arrays.sort(arrayToSort);
        
        for ( int i = 0; i < dimension; i++ ) {
            S.set(i, i, arrayToSort[dimension-1-i]);
        }
        
        return S;
    }

    public DenseMatrix64F getU() {
        return U;
    }

    public DenseMatrix64F getV() {
        return V;
    }

    public DenseMatrix64F getS() {
        return S;
    }
     
}
