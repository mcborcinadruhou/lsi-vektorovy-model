package vwm1;

import java.util.HashMap;
import java.util.List;
import org.ejml.alg.dense.misc.TransposeAlgs;
import org.ejml.alg.dense.mult.CMatrixMatrixMult;
import org.ejml.alg.dense.mult.MatrixMatrixMult;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;
import org.ejml.simple.SimpleMatrix;

public class Query {
    
    SimpleMatrix Q;
    
    public Query(String query, double[] queryWeights, List<String> listOfTerms){
        String[] terms = query.split(",");
        HashMap<String, Double> termsStemmed = new HashMap<>();
        
        int weightAssigner = 0;
        for ( String term : terms ) {
            Stemmer s = new Stemmer();
            s.add(term.toLowerCase().toCharArray(), term.length());
            s.stem();
            String stemmed = s.trim(String.valueOf(s.getResultBuffer(), 0, s.getResultLength()));
            if ( !termsStemmed.containsKey(stemmed) ) {
                termsStemmed.put(stemmed, queryWeights[weightAssigner++]);
                System.out.print(stemmed+" ");
            }
        }
        System.out.println();
        double[][] weights = new double[1][listOfTerms.size()];
        for ( int i = 0; i < listOfTerms.size(); i++ ) {
            System.out.print(listOfTerms.get(i)+" ");
            if ( termsStemmed.containsKey(listOfTerms.get(i)) ) {
                weights[0][i] = termsStemmed.get(listOfTerms.get(i));
            } else {
                weights[0][i] = 0.0;
            }
        }
        
        Q = new SimpleMatrix(weights);
    }
   
    public SimpleMatrix getQ(){
        return this.Q;
    }
}
