package vwm1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ejml.simple.SimpleMatrix;
import static vwm1.Vwm1.readFile;

public class TermByDocMatrix {

    private double[][] matrixData;
    
    public TermByDocMatrix(List<String> files, List<String> terms) {
        HashMap<String, HashMap<String, Double>> termByDocFreqNormalized = termByDocFrequencyNormalized(files);
        HashMap<String, Integer> documentsContainingTerm = numberOfDocsWithTerm(files, terms);
        
        matrixData = new double[terms.size()][files.size()];
        
        for ( int i = 0; i < files.size(); i++ ) {
            for ( int j = 0; j < terms.size(); j++ ) {
                if ( termByDocFreqNormalized.get(files.get(i)).containsKey(terms.get(j)) && documentsContainingTerm.containsKey(terms.get(j)) ) {
                    matrixData[j][i] = computeWeight(termByDocFreqNormalized.get(files.get(i)).get(terms.get(j)), 
                                                documentsContainingTerm.get(terms.get(j)), 
                                                files.size());
                } else {
                    matrixData[j][i] = 0.0;
                }
            }
        }
    }
    
    private HashMap<String, HashMap<String, Double>> termByDocFrequencyNormalized(List<String> files) {
        Stemmer s = new Stemmer();
        HashMap<String, HashMap<String, Integer>> termByDocFreq = new HashMap<>();
        HashMap<String, Integer> fileTerms = new HashMap<>();
        HashMap<String, Integer> max = new HashMap<>();
        List<String> terms = new ArrayList<>();
        
        for ( int i = 0; i < files.size(); i++ ) {
            List<String> file;
            
            try {
                file = readFile(files.get(i));
            } catch (IOException ex) {
                System.out.println("Nepodarilo se otevrit soubor " + files.get(i));
                continue;
            }
            
            fileTerms.clear();

            file.forEach((line) -> {
                String[] words = line.split(" ");
                
                for ( int j = 0; j < words.length; j++ ) {
                    s.add(words[j].toLowerCase().toCharArray(), words[j].length());
                    s.stem();
                    String stemmed = s.trim(String.valueOf(s.getResultBuffer(), 0, s.getResultLength()));
                    
                    if (fileTerms.containsKey(stemmed)) {
                        int count = fileTerms.get(stemmed);
                        fileTerms.put(stemmed, ++count);
                        if ( max.containsKey(stemmed) && count > max.get(stemmed) ) {
                            max.put(stemmed, count);
                        }
                    } else {
                        fileTerms.put(stemmed, 1);
                    }
                    
                    if ( !max.containsKey(stemmed) ) {
                        max.put(stemmed, 1);
                    }
                    
                    if ( !terms.contains(stemmed) ) {
                        terms.add(stemmed);
                    }
                }
            });
            
            termByDocFreq.put(files.get(i), new HashMap<String, Integer>(fileTerms));
        }
        
        HashMap<String, HashMap<String, Double>> termsByDocNormalized = new HashMap<>();
        
        for ( int i = 0; i < files.size(); i++ ) {
            HashMap<String, Integer> doc = termByDocFreq.get(files.get(i));
            HashMap<String, Double> normalized = new HashMap<>();
            for ( String term : terms ) {
                if ( doc.containsKey(term) ) {
                    normalized.put(term, (double)doc.get(term)/(double)max.get(term));
                }
            }
            termsByDocNormalized.put(files.get(i), normalized);
        }
        
        return termsByDocNormalized;
    }
    
    private HashMap<String, Integer> numberOfDocsWithTerm(List<String> files, List<String> terms) {
        HashMap<String, Integer> docsWithTerm = new HashMap<>();
        Stemmer s = new Stemmer();
        
        for ( String file : files ) {
            List<String> lines;
            List<String> wordsStemmed = new ArrayList<>();
            
            try {
                lines = readFile(file);
            } catch (IOException ex) {
                System.out.println("Nepodarilo se otevrit soubor " + file);
                continue;
            }
            
            lines.forEach((line) -> {
                String[] words = line.split(" ");
                for ( String word : words ) {
                    s.add(word.toLowerCase().toCharArray(), word.length());
                    s.stem();
                    String stemmed = s.trim(String.valueOf(s.getResultBuffer(), 0, s.getResultLength()));
                    if ( !wordsStemmed.contains(stemmed)) {
                        wordsStemmed.add(stemmed);
                    }
                }
            });

            for ( String term : terms ) {
                if ( wordsStemmed.contains(term) ) {
                    if ( docsWithTerm.containsKey(term) ) {
                        docsWithTerm.put(term, docsWithTerm.get(term) + 1);
                    } else {
                        docsWithTerm.put(term, 1);
                    }
                }
            }
        }
        
        
        return docsWithTerm;
    }
    
    private double computeWeight(double tf, int df, int n) {
        return tf*Math.log10((double)n/(double)df)/Math.log10(2); // log2(x) = log10(x)/log10(2)
    }
    
    public SimpleMatrix createSimpleMatrix(){
        return new SimpleMatrix(matrixData);
    }
   
}