package vwm1;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.io.File;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javafx.util.Pair;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;
import org.ejml.simple.SimpleMatrix;


public class Vwm1 {
    final static Charset ENCODING = StandardCharsets.UTF_8;
    final static File folder = new File("pokus\\pokus2");
    final static Stemmer stemmer = new Stemmer();
    //final static String stopWordsFile = new String("stopwords.txt");
    //static List<String> stopWords;

    static List<String> readFile(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        return Files.readAllLines(path, ENCODING);
    }

    static List<String> readFiles(File dir) {
        List<String> fileNames = new ArrayList();
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.isDirectory()) {
                    fileNames.addAll(readFiles(child));
                } else {
                    //System.out.println(child.getName());
                    fileNames.add(child.getAbsolutePath());
                }
            }
        }
        return fileNames;
    }
    
    static DenseMatrix64F termToConcept(DenseMatrix64F D, DenseMatrix64F U, DenseMatrix64F S){
        DenseMatrix64F SInverted = new DenseMatrix64F(S);
        CommonOps.invert(SInverted);
        DenseMatrix64F Dtmp = new DenseMatrix64F(SInverted.numRows, U.numRows);
        CommonOps.multTransB(SInverted, U, Dtmp);
        DenseMatrix64F Dnew = new DenseMatrix64F(Dtmp.numRows, D.numCols);
        CommonOps.mult(Dtmp, D, Dnew);
        Dnew.print();
        return Dnew;
    }
        
    static double cosineDistance(DenseMatrix64F a, DenseMatrix64F b) {
        int size = a.getNumCols() > a.getNumRows() ? a.getNumCols() : a.getNumRows();
        
        double dotProduct = 0.0;
        double magnitude1 = 0.0;
        double magnitude2 = 0.0;
        double cosineDisance = 0.0;
        
        for ( int i = 0; i < size; i++ ) {
            dotProduct += a.get(i)*b.get(i);
            magnitude1 += Math.pow(a.get(i), 2);
            magnitude2 += Math.pow(b.get(i), 2);
        }
        
        magnitude1 = Math.sqrt(magnitude1);
        magnitude2 = Math.sqrt(magnitude2);
        
        if ( magnitude1 != 0.0 || magnitude2 != 0.0 ) {
            cosineDisance = dotProduct/(magnitude1*magnitude2);
        } else {
            cosineDisance = 0.0;
        }
        
        return cosineDisance;
    }
    
    public static void main(String[] args) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                new SearchForm().setVisible(true);
            }
        });
        
        
        List<String> files = readFiles(folder);
        HashMap<String, Integer> terms = new HashMap();
        List<String> listOfTerms = new ArrayList<>();
        
        /*try {
            stopWords = readFile(stopWordsFile);
        } catch (IOException ex) {
            System.out.println("Nepodarilo se otevrit soubor " + stopWordsFile);
        }*/
        
        for (int i = 0; i < files.size(); i++) {
            // System.out.println(soubory.get(i));

            List<String> soubor;// new ArrayList();
            try {
                soubor = readFile(files.get(i));
            } catch (IOException ex) {
                System.out.println("Nepodarilo se otevrit soubor " + files.get(i));
                continue;
            }
            soubor.forEach((line) -> {
                String[] words = line.split(" ");
                for (int j = 0; j < words.length; j++) {
                    stemmer.add(words[j].toLowerCase().toCharArray(), words[j].length());
                    stemmer.stem();
                    String stemmed = stemmer.trim(String.valueOf(stemmer.getResultBuffer(), 0, stemmer.getResultLength()));
                    //if ( !stopWords.contains(stemmed) ) {
                        if (terms.containsKey(stemmed)) {
                            int count = terms.get(stemmed);
                            terms.put(stemmed, count + 1);
                        } else {
                            terms.put(stemmed, 1);
                        }
                        
                        if ( !listOfTerms.contains(stemmed) ) {
                            listOfTerms.add(stemmed);
                        }
                    //}
                    //System.out.println(words[j]);
                }
            }
            );

        }/*
        int slov = 0;
        for (Map.Entry<String, Integer> entry : terms.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();
            //   System.out.println(key + " x "+ value);
            slov += value;
            // do what you have to do here
            // In your case, an other loop.
        }
        */
        
        /*Object[] a = terms.entrySet().toArray();
        Arrays.sort(a, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Map.Entry<String, Integer>) o2).getValue()
                        .compareTo(((Map.Entry<String, Integer>) o1).getValue());
            }
        });
        for (Object e : a) {
            System.out.println(((Map.Entry<String, Integer>) e).getKey() + " : "
                    + ((Map.Entry<String, Integer>) e).getValue());
        }*/
        
        TermByDocMatrix termByDoc = new TermByDocMatrix(files, listOfTerms);
        ConceptByDocMatrix conceptByDoc = new ConceptByDocMatrix(termByDoc);

        Query query = new Query(new String("work,shovel"), new double[]{1.0, 1.0}, listOfTerms);
        DenseMatrix64F UK = conceptByDoc.getU();
        DenseMatrix64F SK = conceptByDoc.getS();
        DenseMatrix64F VK = conceptByDoc.getV();
        UK.reshape(UK.getNumRows(), 3);
        SK.reshape(3, 3);
        VK.reshape(3, VK.getNumCols());
        DenseMatrix64F conceptQueryVector = termToConcept(query.getQ().transpose().getMatrix(), UK, SK);

        SimpleMatrix termByDocMatrix = termByDoc.createSimpleMatrix();
        List<Pair<String, DenseMatrix64F>> conceptsVectors = new ArrayList<>();
        for ( int i = 0; i < termByDocMatrix.numCols(); i++ ) {
            conceptsVectors.add(new Pair<>(files.get(i), termToConcept(termByDocMatrix.extractVector(false, i).getMatrix(), UK, SK)));
        }
        
        for ( Pair<String, DenseMatrix64F> conceptVector : conceptsVectors ) {
            System.out.println(conceptVector.getKey()+":"+cosineDistance(conceptVector.getValue(), conceptQueryVector));
        }
        
        
        

        //System.out.println(slov);
        /*
         
        List<String> soubor = new ArrayList();
        try {
            soubor = readFile(FILE_NAME);
        } catch (IOException ex) {
            System.out.println("Nepodarilo se otevrit soubor " + FILE_NAME);
        }
        soubor.forEach((line) -> {
                String[] words = line.split(" ");
                for(int i = 0; i < words.length; i++){
                    System.out.println(words[i]);  
                }
            }
        );
         */
    }

}
